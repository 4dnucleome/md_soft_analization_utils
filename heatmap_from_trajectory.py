#!/usr/bin/env python3

import argparse
import os

import mdtraj as md
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.colors import SymLogNorm
from scipy.spatial.distance import pdist, squareform
from tqdm import tqdm


class HeatmapCalculator(object):
    def __init__(self, trajectory: np.ndarray):
        self.xyz = trajectory
        self.n_steps = self.xyz.shape[0]
        self.n_beads = self.xyz.shape[1]
        self.distance_threshold = 0.15  # nanometers
        self.hm = np.zeros((self.n_beads, self.n_beads), dtype=np.int)

    def make_contact_map(self, frame_index: int):
        dm = squareform(pdist(self.xyz[frame_index], metric='euclidean'))
        cm = (dm < self.distance_threshold).astype(np.int)
        return cm

    def make_heatmap(self):
        for i in tqdm(range(self.n_steps)):
            self.hm += self.make_contact_map(i)

    def save_heatmap(self, path):
        head, tail = os.path.split(path)
        if head and not os.path.exists(head):
            os.mkdir(head)
        np.savez_compressed(path, self.hm)

    def display_heatmap(self):
        plt.imshow(self.hm, origin='lower', norm=SymLogNorm(linthresh=1))
        plt.colorbar()
        plt.show()

    def calc_stats(self):
        zeros = self.hm.size - np.count_nonzero(self.hm)
        zeros_density = zeros / self.hm.size
        print("heatmap range:", self.hm.min(initial=float('inf')), self.hm.max(initial=float('-inf')))
        print(f"heatmap zeros: {zeros} ({zeros_density * 100:0.2f} %)", )


def run(args: argparse.Namespace):
    xyz = md.load(args.trajectory, top=args.topology).xyz
    heatcalc = HeatmapCalculator(xyz)
    heatcalc.make_heatmap()
    heatcalc.calc_stats()
    heatcalc.save_heatmap(args.out_file)
    print(args.out_file, 'saved.')


def main():
    parser = argparse.ArgumentParser(description="Saves heatmap from DCD trajectory.")
    parser.add_argument('trajectory', help="trajectory in DCD")
    parser.add_argument('topology', help='topology in PSF')
    parser.add_argument('out_file', help="npz file with heatmap")
    args = parser.parse_args()
    run(args)


if __name__ == '__main__':
    main()
