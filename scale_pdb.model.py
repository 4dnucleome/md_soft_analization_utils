#!/usr/bin/env python3

from points_io import point_reader, save_points_as_pdb

import argparse


def main():
    parser = argparse.ArgumentParser(description="Scale structure")
    parser.add_argument('infile', help='pdb file')
    parser.add_argument('scale', type=float, help='scale (float)')
    parser.add_argument('outfile', help='pdb file')
    args = parser.parse_args()

    points = point_reader(args.infile)
    points = points * args.scale
    save_points_as_pdb(points, args.outfile)


if __name__ == '__main__':
    main()
