#!/usr/bin/env python3

import argparse
from typing import List

import mdtraj as md
import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from scipy.stats import pearsonr, norm


class AutoCorrCalc(object):
    def __init__(self, velocities: np.ndarray):
        self.data: np.ndarray = velocities
        self.n_steps: int = velocities.shape[0]
        self.n_beads: int = velocities.shape[1]
        self.data_norm: np.ndarray = None
        self.correlations: List[float] = []
        self.q1 = None
        self.q50 = None
        self.q99 = None
        self.p1 = None
        self.q1 = None

    def calculate_auto_correlation(self):
        if not hasattr(self, 'data_normed'):
            self.data_norm = np.linalg.norm(self.data, axis=2)
        ref = np.linalg.norm(self.data[0], axis=1)
        for i in self.data_norm:
            r, _ = pearsonr(ref, i)
            self.correlations.append(r)
        self._calc_stats()

    def _calc_stats(self):
        self.mean = np.mean(self.correlations)
        self.std = np.std(self.correlations)
        self.q1 = np.percentile(self.correlations, q=1)
        self.q50 = np.percentile(self.correlations, q=50)
        self.q99 = np.percentile(self.correlations, q=99)
        p_val = 0.001
        self.p001 = norm.ppf(p_val, self.mean, self.std)
        self.p999 = norm.ppf(1 - p_val, self.mean, self.std)

    def show_autocorrelation_plot(self):
        if self.correlations:
            plt.plot(self.correlations)
            plt.title('Velocities autocorrelation')
            plt.xlabel('Trajectory frame')
            plt.ylabel('Correlation')
            plt.grid()

            y_min, y_max = plt.gca().get_ylim()

            self._mark_interval(self.p001, self.p999, 'ppf(.001)', 'ppf(.999)', 'gray', ':')

            ax = plt.gca()
            divider = make_axes_locatable(ax)
            ax_hist = divider.append_axes("right", size=1, pad=0.1, sharey=ax)
            ax_hist.tick_params(labelleft=False)
            ax_hist.hist(self.correlations, orientation="horizontal", bins=int(self.n_steps ** 0.6), density=True)

            x = np.linspace(y_min, y_max, 600)
            y = norm.pdf(x, self.mean, self.std)
            ax_hist.plot(y, x)

            plt.sca(ax_hist)
            plt.grid()
            plt.show()
        else:
            raise RuntimeError('self.corr is empty!')

    def _mark_interval(self, a, b, prefix1, prefix2, color, ls):
        font_dict = dict(fontsize=8, horizontalalignment='right')
        offset = 0.1 * self.q1
        plt.hlines(a, xmin=0, xmax=self.n_steps, colors=color, ls=ls, )
        plt.text(x=self.n_steps, y=a + offset, s=f"{prefix1}={a:0.2f}", verticalalignment='top', **font_dict)
        plt.hlines(b, xmin=0, xmax=self.n_steps, colors=color, ls=ls)
        plt.text(x=self.n_steps, y=b - offset, s=f"{prefix2}={b:0.2f}", verticalalignment='bottom', **font_dict)

    def describe_autocorrelation_distribution(self):
        print(f"Correlations p1 / p50 / p99: {self.q1:0.2f} / {self.q50:0.2f} / {self.q99:0.2f}")


def run(args: argparse.Namespace):
    h5file = md.formats.HDF5TrajectoryFile(args.state)
    velocities = np.array(h5file.root.velocities)
    h5file.close()
    auto_corr = AutoCorrCalc(velocities)
    auto_corr.calculate_auto_correlation()
    auto_corr.show_autocorrelation_plot()
    auto_corr.describe_autocorrelation_distribution()


def main():
    parser = argparse.ArgumentParser(description="Inspect autocorrelation time from trajectory.")
    parser.add_argument('state', help="velocities in h5")
    args = parser.parse_args()
    run(args)


if __name__ == '__main__':
    main()
