from genomic_datatypes import BedpeRecord, BedRecord, BedList, BedpeList


def read_bedpe(filename: str) -> BedpeList:
    records: BedpeList = BedpeList()
    with open(filename) as f:
        for line in f:
            c1, b1, e1, c2, b2, e2, v, *_ = line.strip().split()
            b1, e1, b2, e2, v = map(int, [b1, e1, b2, e2, v])
            records.append(BedpeRecord(c1, b1, e1, c2, b2, e2, v))
    return records


def read_bed(filename: str) -> BedList:
    records: BedList[BedRecord] = BedList()
    with open(filename) as f:
        for line in f:
            c, b, e, *_ = line.strip().split()
            b, e = map(int, [b, e])
            records.append(BedRecord(c, b, e))
    return records
