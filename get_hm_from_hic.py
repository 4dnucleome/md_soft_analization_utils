#!/usr/bin/env python3

import argparse
import re
import struct
import sys
import straw


class ParsingError(Exception):
    pass


def read_c_str(f):
    buf = ""
    while True:
        b = f.read(1)
        b = b.decode('utf-8', 'backslashreplace')
        if b is None or b == '\0':
            return str(buf)
        else:
            buf = buf + b


def read_header(filename: str):
    with open(filename, 'rb') as req:
        magic_string = struct.unpack('<3s', req.read(3))[0]
        req.read(1)
        if magic_string != b"HIC":
            print('This does not appear to be a HiC file magic string is incorrect')
            sys.exit(1)
        version = struct.unpack('<i', req.read(4))[0]
        master_index = struct.unpack('<q', req.read(8))[0]

        genome = ""
        c = req.read(1).decode("utf-8")
        while c != '\0':
            genome += c
            c = req.read(1).decode("utf-8")
        print('HiC version: {}'.format(str(version)))
        print('Master index: {}'.format(str(master_index)))
        print('Genome ID: {}'.format(str(genome)))
        print('Attribute dictionary:')
        n_attributes = struct.unpack('<i', req.read(4))[0]
        for x in range(0, n_attributes):
            key = read_c_str(req)
            value = read_c_str(req)
            print('  {}: {}'.format(key, value))
        n_chrs = struct.unpack('<i', req.read(4))[0]
        print("Chromosomes: ")
        for x in range(0, n_chrs):
            name = read_c_str(req)
            length = struct.unpack('<i', req.read(4))[0]
            print('  {0}  {1}'.format(name, length))
        n_bp_res = struct.unpack('<i', req.read(4))[0]
        print("Base pair-delimited resolutions: ")
        res = []
        for x in range(0, n_bp_res):
            res.append(struct.unpack('<i', req.read(4))[0])
        res = [str(i) for i in res]
        res = ' '.join(res)
        print(res)


def parse_si(s: str) -> int:
    m = re.match(r'^(?P<value>\d*[.,]?\d*)(?P<suffix>[kMG])?$', s)
    if m:
        v = float(m.group('value'))
        suffix = m.group('suffix')
        if suffix == 'k':
            return int(1000 * v)
        elif suffix == 'M':
            return int(1e6 * v)
        elif suffix == 'G':
            return int(1e9 * v)
        else:
            return int(v)
    else:
        raise ParsingError('Could not parse resolution: {}'.format(s))


def parse_loc(s):
    m = re.match(r'^(\d{1,2}|[XYM])[:\-_](\d+)[:\-_](\d+)$', s)
    if m:
        _, begin, _ = m.groups()
        begin = int(begin)
        return begin
    else:
        m = re.match(r'^(\d{1,2}|[XYM])$', s)
        if m:
            return 1
        else:
            raise ParsingError('Could not parse range or chromosome id: {}'.format(s))


class InfoAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        read_header(values[0])
        parser.exit()


class ParseSI(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, parse_si(values))


def convert(args: argparse.Namespace):
    result = straw.straw('NONE', args.infile, args.loc, args.loc, 'BP', args.resolution)
    # the values returned are in x / y / counts
    print(result)
    loc_begin = parse_loc(args.loc)
    restraints = []
    for i in range(len(result[0])):
        begin, end, count = (result[0][i] - loc_begin) // args.resolution + 1, (result[1][i] - loc_begin) // args.resolution + 1, result[2][i]
        if abs(begin - end) > 2 and count >= args.limit and (begin, end) not in restraints:
            restraints.append((begin, end))
    restraints.sort(key=lambda x: x[0])
    print("Number of interactions for count {}: {}".format(args.limit, len(restraints)))
    w = ''
    for i in restraints:
        begin, end = i
        w += '{}\t{}\n'.format(begin, end)
    w = w[:-1]
    with open(args.outfile, 'w') as f:
        f.write(w)
    print('Coordinates written in {}...'.format(args.outfile))


def main():
    parser = argparse.ArgumentParser(description='Converts hic file into three column txt file')
    parser.register('action', 'info', InfoAction)
    parser.register('action', 'parse_si', ParseSI)
    parser.add_argument('-i', '--info', action='info', nargs=1, help='print hic file header and exit.')
    parser.add_argument('-l', '--limit', default=1, type=int, help='Skip bins with Count lower than this limit [default: 1]')
    parser.add_argument('resolution', action='parse_si', help='resolution in bp. SI k M G suffixes allowed (e.g. 2M 2k 1G')
    parser.add_argument('loc', help='Chromosome name and (optionally) range, i.e. "1" or "1:10000:25000"')
    parser.add_argument('infile', help='input file')
    parser.add_argument('outfile', help='output file')
    args = parser.parse_args()
    convert(args)


if __name__ == '__main__':
    main()
