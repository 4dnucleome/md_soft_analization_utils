import argparse

import mdtraj as md
from nma import ANMA


def main():
    """Based on this tutorial: http://mdtraj.org/1.9.3/examples/pca.html"""
    parser = argparse.ArgumentParser(description="Perform NMA analysis of trajectory.")
    parser.add_argument('pdb_file')
    args = parser.parse_args()

    pdb = md.load_pdb(args.pdb_file)

    # Initialize ANMA object
    anma = ANMA(mode=0, rmsd=0.15, n_steps=50, selection='all')

    # Transform the PDB into a short trajectory of a given mode
    anma_traj = anma.fit_transform(pdb)
    anma_traj.save('dupa_0.pdb')

    anma = ANMA(mode=1, rmsd=0.15, n_steps=50, selection='all')

    # Transform the PDB into a short trajectory of a given mode
    anma_traj = anma.fit_transform(pdb)
    anma_traj.save('dupa_1.pdb')

    # Transform the PDB into a short trajectory of a given mode
    anma_traj = anma.fit_transform(pdb)
    anma_traj.save('dupa_0.pdb')

    anma = ANMA(mode=2, rmsd=0.15, n_steps=50, selection='all')

    # Transform the PDB into a short trajectory of a given mode
    anma_traj = anma.fit_transform(pdb)
    anma_traj.save('dupa_2.pdb')


if __name__ == '__main__':
    main()
