#!/usr/bin/env python

import argparse

import numpy as np
from matplotlib import pyplot as plt


def main():
    parser = argparse.ArgumentParser(description="Display npz heatmap, or save as file")
    parser.add_argument('infile', help="npz file with heatmap (numpy compressed array)")
    parser.add_argument('-o', '--outfile', help="PNG file")
    args = parser.parse_args()

    data = np.load(args.infile)['arr_0']
    plt.imshow(data, origin='lower', vmax=np.percentile(data, 95))
    if args.outfile:
        plt.savefig(args.outfile)
    else:
        plt.show()


if __name__ == '__main__':
    main()
