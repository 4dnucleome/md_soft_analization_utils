from dataclasses import dataclass, field
from typing import Union

chromosomes = [f"chr{i}" for i in list(range(1, 23)) + ["X", "Y", "M"]]


class InterChromosomalError(Exception):
    """Raise this when there supposed to be all records from the same chromosome."""
    pass


@dataclass(unsafe_hash=True)
class BedRecord:
    chr: str  # string with leading 'chr'
    a: int
    b: int
    seq: int = field(init=False)  # artificial field used for sorting by chromosome id.

    @property
    def midpoint(self) -> int:
        return (self.a + self.b) // 2

    @property
    def length(self) -> int:
        return self.__len__()

    def __len__(self):
        return self.b - self.a

    def as_bed(self) -> str:
        return f"{self.chr}\t{self.a}\t{self.b}"

    def as_coords(self) -> str:
        return f"{self.chr}:{self.a}-{self.b}"

    def __post_init__(self):
        self.seq = chromosomes.index(self.chr)
        if self.a > self.b:
            raise ValueError(f"In BED record begin must be smaller than end: {self.as_coords()}")

    def __eq__(self, other):
        return self.chr == other.chr and self.a == other.a and self.b == other.b

    def __repr__(self):
        return f"[{self.as_coords()}]"


@dataclass(unsafe_hash=True)
class BedpeRecord(object):
    c1: str  # chromosome 1 ID
    b1: int  # begin 1
    e1: int  # end 1
    c2: str  # chromosome 2 ID
    b2: int  # begin 2
    e2: int  # end 2
    v: Union[int, float]  # value (PET-Count)
    seq: int = field(init=False)  # artificial field used for sorting

    def __post_init__(self):
        self.seq = chromosomes.index(self.c1)

    @property
    def a(self) -> int:
        return (self.e1 + self.b1) // 2

    @property
    def b(self) -> int:
        return (self.e2 + self.b2) // 2

    def get_anchors(self):
        return BedRecord(self.c1, self.b1, self.e1), BedRecord(self.c2, self.b2, self.e2)

    def __len__(self) -> int:
        return self.b - self.a

    @property
    def length(self) -> int:
        return self.__len__()

    def __str__(self) -> str:
        return f"{self.c1}\t{self.b1}\t{self.e1}\t{self.c2}\t{self.b2}\t{self.e2}\t{self.v}"

    def same_anchors(self, other) -> bool:
        """Checks if two interactions share the same anchors."""
        return self.c1 == other.c1 and self.b1 == other.b1 and self.e1 == other.e1 and \
               self.c2 == other.c2 and self.b2 == other.b2 and self.e2 == other.e2


class BedList(list):
    def save_bed_file(self, outfile: str):
        w = '\n'.join((i.as_bed() for i in self))
        with open(outfile, 'w') as f:
            f.write(w)
        print(f"{outfile} saved.")

    def save_anchors_file(self, outfile: str):
        w = '\n'.join((i.as_coords() for i in self))
        with open(outfile, 'w') as f:
            f.write(w)
        print(f"{outfile} saved.")


class BedpeList(list):
    def save_bedpe_file(self, outfile: str):
        w = '\n'.join(str(i) for i in self)
        with open(outfile, 'w') as f:
            f.write(w)
        print(f"{outfile} saved.")

    def get_anchors(self) -> BedList:
        """Sorting based on anchor midpoint."""
        anchors = set()
        for i in self:
            a1, a2 = i.get_anchors()
            anchors.add(a1)
            anchors.add(a2)
        anchors = BedList(anchors)
        anchors.sort(key=lambda x: (x.seq, x.midpoint))
        return anchors


class BedpeDict(dict):
    def __init__(self, filename):
        super().__init__()
        for i in list(range(1, 23)) + ['X', 'Y', 'M']:
            self[f'chr{i}'] = BedpeList()
        with open(filename) as f:
            for line in f:
                c1, b1, e1, c2, b2, e2, val = line.split()
                b1, e1, b2, e2, val = map(int, [b1, e1, b2, e2, val])
                self[c1].append(BedpeRecord(c1, b1, e1, c2, b2, e2, val))
        for i in self:
            self[i].sort(key=lambda x: (x.b1, x.e1, x.b2, x.e2))

    def __len__(self) -> int:
        count = 0
        for i in self:
            count += len(self[i])
        return count
