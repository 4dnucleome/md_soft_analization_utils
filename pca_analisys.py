import argparse
from itertools import combinations

import matplotlib.pyplot as plt
import mdtraj as md
from sklearn.decomposition import PCA


def main():
    """Based on this tutorial: http://mdtraj.org/1.9.3/examples/pca.html"""
    parser = argparse.ArgumentParser(description="Perform PCA analysis of trajectory.")
    parser.add_argument('trajectory')
    parser.add_argument('topology')
    args = parser.parse_args()

    trj = md.load(args.trajectory, top=args.topology)
    pca1 = PCA(n_components=2)
    trj.superpose(trj, 0)
    reduced_cartesian = pca1.fit_transform(trj.xyz.reshape(trj.n_frames, trj.n_atoms * 3))

    plt.figure()
    plt.scatter(reduced_cartesian[:, 0], reduced_cartesian[:, 1], marker='x', c=trj.time)
    plt.xlabel('PC1')
    plt.ylabel('PC2')
    plt.title('Cartesian coordinate PCA')
    cbar = plt.colorbar()
    cbar.set_label('Time')
    plt.show()
    print('done pca 1')

    pca2 = PCA(n_components=2)
    atom_pairs = list(combinations(range(trj.n_atoms), 2))
    pairwise_distances = md.geometry.compute_distances(trj, atom_pairs)
    reduced_distances = pca2.fit_transform(pairwise_distances)
    plt.figure()
    plt.scatter(reduced_distances[:, 0], reduced_distances[:, 1], marker='x', c=trj.time)
    plt.xlabel('PC1')
    plt.ylabel('PC2')
    plt.title('Pairwise distance PCA')
    cbar = plt.colorbar()
    cbar.set_label('Time')
    plt.show()
    print('done pca 2')


if __name__ == '__main__':
    main()
