import argparse

from matplotlib import pyplot as plt


def main():
    parser = argparse.ArgumentParser(description="Make histogram of data from file in format UCSF Chimera trajectory dumps (eg. dihedral angles)")
    parser.add_argument("infile", help="dump file")
    args = parser.parse_args()

    with open(args.infile) as f:
        next(f)
        next(f)
        values = []
        for i in f:
            _, v = i.split()
            values.append(float(v.replace(',', '.')))
    plt.hist(values)
    plt.show()


if __name__ == '__main__':
    main()
