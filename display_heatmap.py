#!/usr/bin/env python

import argparse

import numpy as np
from matplotlib import pyplot as plt
from matplotlib.colors import SymLogNorm
from matplotlib.ticker import MultipleLocator


class HeatmapPlotter(object):
    def __init__(self, hm: np.ndarray):
        self.hm = hm

    def show_hm(self):
        plt.figure(figsize=(6.4, 4.8), dpi=150)
        plt.imshow(self.hm, origin='lower', norm=SymLogNorm(linthresh=1))
        plt.colorbar()
        ax = plt.gca()
        ax.xaxis.set_minor_locator(MultipleLocator(10))
        ax.yaxis.set_minor_locator(MultipleLocator(10))
        plt.show()

    def show_sig(self):
        n = self.hm.shape[0]
        diags = []
        for i in range(n):
            diags.append(np.sum(np.diag(self.hm, i))/(n-i))
        diags = np.array(diags)
        diags = diags / np.max(diags)
        plt.loglog(diags)
        plt.grid(which="both")
        plt.xlabel("Position separation")
        plt.ylabel("Contact probability $P_c$")
        plt.show()


def main():
    parser = argparse.ArgumentParser(description="Shows heatmap saved as npz compressed file")
    parser.add_argument('infile', help="NPZ compressed file with heatmap")
    args = parser.parse_args()
    with np.load(args.infile) as data:
        hm = data[list(data.keys())[0]]
    hp = HeatmapPlotter(hm)
    hp.show_sig()


if __name__ == '__main__':
    main()
