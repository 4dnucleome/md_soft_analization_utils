import argparse
import os
from typing import Dict, Union

import numpy as np
from matplotlib import pyplot as plt

from genomic_datatypes import BedpeList
from genomic_io import read_bedpe


def get_lengths_of_interactions(data: BedpeList) -> np.ndarray:
    """Returns np array of interaction lengths."""
    lengths = []
    for i in data:
        lengths.append(len(i))
    lengths = np.array(lengths)
    return lengths


def get_values_of_interactions(data: BedpeList) -> np.ndarray:
    """Return np array of PET-Counts values."""
    values = []
    for i in data:
        values.append(i.v)
    values = np.array(values)
    return values


def calculate_stats(data: np.ndarray) -> Dict[str, Union[int, float]]:
    """Calculates several simple descriptive statistics for generic data vector."""
    stats = dict(min=np.min(data), max=np.max(data))
    stats['range'] = stats['max'] - stats['min']
    stats['mean'] = np.mean(data)
    stats['median'] = np.median(data)
    stats['stdev'] = np.std(data, ddof=1)
    stats['varcoef'] = stats['stdev'] / stats['mean'] * 100
    return stats


def print_stats(data: Dict[str, Union[int, float]]) -> None:
    """Prints pre-calculated stats."""
    print(f"  min = {data['min']}")
    print(f"  max = {data['max']}")
    print(f"  range R = {data['range']}")
    print(f"  mean ̅x = {data['mean']:,.1f}")
    print(f"  median mₑ = {data['median']:,.0f}")
    print(f"  Standard deviation¹ σ = {data['stdev']:,.0f}")
    print(f"  coefficient of variation ν =  {data['varcoef']:,.1f} %")
    print('  ¹ squared root of unbiased variation estimator.')


def histogram(data: np.ndarray, out_dir, out_file_name, title=''):
    plt.clf()
    hist, bins, _ = plt.hist(data)
    print("=== Histogram ===")
    print(f"Number of bins: {len(hist):>8}")
    print(f"Bin size:       {bins[1] - bins[0]:>8,}")
    for i in range(len(hist)):
        print(f"{i + 1:>2}: {bins[i]:>10,.1f} -{bins[i + 1]:>10,.1f} : n = {int(hist[i])}")
    plt.title(title)
    plt.grid()
    plt.savefig(os.path.join(out_dir, out_file_name))


def pet_counts_histogram(data: np.ndarray, out_dir, out_file_name, title=''):
    plt.clf()
    bins = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16])
    hist, bins, _ = plt.hist(data, bins=bins)
    print("=== Histogram ===")
    for i in range(len(hist)):
        print(f"{i + 1:>2}: n = {int(hist[i])}")

    print(f"16 and more: n = {len(data[data > 16])}")
    plt.title(title)
    plt.xticks(bins[:-1] + 0.5, bins[:-1])
    plt.savefig(os.path.join(out_dir, out_file_name))


def main():
    parser = argparse.ArgumentParser(description="Calculate some descriptive statistics for BEDPE interactions file. Single chromosome required!")
    parser.add_argument('input', help="BEDPE file")
    parser.add_argument('output', help="directory where report will be saved")
    args = parser.parse_args()

    # Make output directory
    if not os.path.isdir(args.output):
        os.mkdir(args.output)

    data = read_bedpe(args.input)
    print(f"Number of interactions N = {len(data)}")
    print()

    # Lengths of interactions
    lengths = get_lengths_of_interactions(data)
    lengths_stats = calculate_stats(lengths)
    print("=== Interactions lengths ===")
    print_stats(lengths_stats)
    histogram(lengths, args.output, 'histogram_of_lengths.png', title="Histogram of lengths")
    print()

    # Values of interactions (PET-Counts)
    values = get_values_of_interactions(data)
    values_stats = calculate_stats(values)
    print("=== Interactions values ===")
    print_stats(values_stats)
    pet_counts_histogram(values, args.output, 'histogram_of_PET-Counts.png', title="Histogram of PET-Counts")


if __name__ == '__main__':
    main()
